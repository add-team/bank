import json
import web3

from web3 import Web3, HTTPProvider, TestRPCProvider
from solc import compile_source
from web3.contract import ConciseContract


web3 = Web3(HTTPProvider())

account = web3.eth.account.privateKeyToAccount('0x4aa4509a5853a64507f0e9e9f3750d902e91f88a257211a7057b9d6a6fd45c84')
web3.eth.defaultAccount = account.address


challenge_contract_address = '0xF588D260eEf9c65f6205107bCd9f7E7f6aBbE3e0'

import json

truffleFile = json.load(open('./build/contracts/NewBank.json'))

abi = truffleFile['abi']


def send_transaction_sync(tx, args={}):
    args['nonce'] = web3.eth.getTransactionCount(account.address)
    signed_txn = account.signTransaction(tx.buildTransaction(args))
    tx_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)
    return web3.eth.waitForTransactionReceipt(tx_hash)

c = web3.eth.contract(address=challenge_contract_address, abi=abi)

print(c.functions.version().call())

#tx = c.functions.deposit(0,2,1)
#send_transaction_sync(tx,  {'gas':6000000})
