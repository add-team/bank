import json
import web3
import time
from  datetime import datetime
from web3 import Web3, HTTPProvider, TestRPCProvider
from solc import compile_source
from web3.contract import ConciseContract


web3 = Web3(HTTPProvider())

account = web3.eth.account.privateKeyToAccount('0x4aa4509a5853a64507f0e9e9f3750d902e91f88a257211a7057b9d6a6fd45c84')
web3.eth.defaultAccount = account.address


challenge_contract_address = '0xfd1F9bd2a16FDb56B6c9655D711Bd505926eD962'

import json

truffleFile = json.load(open('./build/contracts/Bank.json'))

abi = truffleFile['abi']
bytecode = truffleFile['bytecode']


def send_transaction_sync(tx, args={}):
		args['nonce'] = web3.eth.getTransactionCount(account.address)
		signed_txn = account.signTransaction(tx.buildTransaction(args))
		tx_hash = web3.eth.sendRawTransaction(signed_txn.rawTransaction)
		return web3.eth.waitForTransactionReceipt(tx_hash)

c = web3.eth.contract(address=challenge_contract_address, abi=abi)


event_dep = c.eventFilter('NewDepositOpen', {'fromBlock': 0,'toBlock': 'latest'}) 
event_cred = c.eventFilter('NewCreditOpen', {'fromBlock': 0,'toBlock': 'latest'}) 

poll_interval = 2

def handle_event(event):
		print(event.args)


class Entry:
	
	def __init__(self, id, time):
		self.id = id
		self.time = time

deps = []
creds = []


while True:
  for event in event_dep.get_new_entries():
    id = event.args.depositAccountId
    data = c.functions.depositAccountsMap(id).call()
    deps.append(Entry(id, data[6] + data[7] * 60))
    print('New deposit')
    
  for event in event_cred.get_new_entries():
    id = event.args.creditAccountId
    data = c.functions.creditAccountsMap(id).call()
    creds.append(Entry(id, data[5] + data[6] * 60))
    print('New credit')

	 
  time.sleep(poll_interval)
  
  now = int(time.time())
  deps.sort(key= lambda x: x.time)
  while len(deps) > 0 and deps[0].time <= now:
    print(deps[0].id, deps[0].time, now)
    tx = c.functions.withdraw(deps[0].id)
    send_transaction_sync(tx, {'gas':6000000})
    del deps[0]
    
  creds.sort(key= lambda x: x.time)
  while len(creds) > 0 and creds[0].time <= now:
    print(creds[0].id, creds[0].time, now)
    tx = c.functions.payback(deps[0].id, 0)
    send_transaction_sync(tx, {'gas':6000000})
    del creds[0]
  
   
   
