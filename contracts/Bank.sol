pragma solidity 0.4.24;

import "./Managable.sol";

import "openzeppelin-solidity/contracts/ownership/Ownable.sol";
import "openzeppelin-solidity/contracts/token/ERC20/IERC20.sol";
import "openzeppelin-solidity/contracts/math/SafeMath.sol";


contract Bank is Ownable, Managable {
    using SafeMath for uint256; //https://ethereum.stackexchange.com/questions/25829/meaning-of-using-safemath-for-uint256

    //event-ы


    event NewDepositOpen(address indexed client, uint256 depositAccountId);
    event DepositWithdraw(address indexed client, uint256 depositAccountId);
    event EarlyDepositWithdraw(address indexed client, uint256 depositAccountId);

    event NewCreditOpen(address indexed client, uint256 creditAccountId);
    event CreditClosed(address indexed client, uint256 creditAccountId);                //погашен
    event CreditPartiallyOffset(address indexed client, uint256 creditAccountId);       //частично погашен


    event NewCreditProductAdded(uint256 creditProductId);
    event NewDepositProductAdded(uint256 depositProductId);

    event NewBlackListEntryAdded(uint256 blackListEntryIndex);

    event BlackListEntryDeleted(uint256 blackListEntryIndex);

    event ToppedUp(uint256 amount);

    event TookProfit(uint256 amount);

    event CreditRatingEdited(uint16 rating);

    event ClientAdded(address clientAddress);



    //структуры

    struct CreditProduct { //кредитный продукт
        bool isActual;
        address token; //адрес токена для которого существует этот продукт  
        uint256 index;    

        uint256 minAmount;
        uint256 maxAmount;

        uint8 minDuration; //в минутах (минута - эквивалент года)
        uint8 maxDuration;

        uint16 rate; // процентная ставка от 0 до 10000, задает сотые доли процента. 100 rate = 1%
        string name;
    }

    struct DepositProduct { //депозитный продукт
        bool isActual;
        address token; //адрес токена для которого существует этот продукт 
        uint256 index;     

        uint256 minAmount;
        uint256 maxAmount;

        uint8 minDuration;
        uint8 maxDuration;

        uint16 rate; // процентная ставка от 0 до 10000, задает сотые доли процента. 100 rate = 1%
        string name;
    }

    enum CreditAccountState { Opened, Closed, Overdued }
    struct CreditAccount { //кредитный счёт
        CreditAccountState state;        //открыт, закрыт, просрочен
        address client;

        uint256 index;
        uint256 productId; //какой продукт
        
        uint256 balance; //состояние счёта

        uint256 startDate; //дата открытия счёта в секундах с начала века
        uint8 duration; //срок кредита в минутах
    }

    struct DepositAccount { //кредитный счёт
        bool isOpened;
        address client;

        uint256 index;
        uint256 productId; //какой продукт
        
        uint256 balance; //состояние счёта
        uint256 startBalance; 

        uint256 startDate; //дата открытия счёта
        uint8 duration; //срок депозита в минутах 
    }

    struct Client { // клиент
        bool exists;            //для проверки, есть ли уже такой пользователь

        uint256[] creditAccountIds; //id кредитных счетов клиента
        uint256[] depositAccountIds; //id депозитных счетов клиента

        uint16 rating; // кредитный рейтинг
    }

    struct BlackListEntry {
        uint256 index;

        uint256 debt;
        uint256 defaultDate;
    }

    struct SupportedToken {
        bool exists;

        address tokenAddress;
        string name;
        string symbol;
    }

    //mapping-и и массивы

    mapping (uint256 => DepositProduct) public depositProductsMap; //все могут посмотреть наши тарифы на депозиты
    uint256[] public actualDepositProductIds;                         //счетчик нужен для получения всех элементов mapping-а
    uint256[] public archivedDepositProductIds;

    mapping(uint256 => CreditProduct) public creditProductsMap; //и кредиты
    uint256[] public actualCreditProductIds;
    uint256[] public archivedCreditProductIds;

    mapping (uint256 => CreditAccount) public creditAccountsMap; //кредитные счета банка
    uint256[] private _openedCreditAccountIds;
    uint256[] private _closedCreditAccountIds;
    uint256[] private _overdueCreditAccountIds;

    mapping (uint256 => DepositAccount) public depositAccountsMap; //депозитные счета банка
    uint256[] private _openedDepositAccountIds;
    uint256[] private _closedDepositAccountIds;

    mapping (address => Client) private _clientsMap; //клиенты банка
    address[] private _clients;           //нужно для получения всех клиентов банка

    mapping (address => BlackListEntry) private _blackListEntriesMap;
    address[] private _blackList;
    
    mapping (address => SupportedToken) public supportedTokensMap;
    address[] public supportedTokenAddresses;


    //синдикация начало
    struct Range {
        uint256 end;
        uint256 amount;
    }
    mapping (address => mapping(uint256 => Range)) private _token_syndication_balances;
    mapping( address => uint256) private _token_last_time;  

    mapping (address => mapping(uint256 => Range)) private _token_capital_balances;
    mapping( address => uint256) private _token_capital_last_time;  

    function _count_product(address _token, uint256 _amount, uint256 _duration) internal {
        mapping(uint256 => Range) _balances = _token_syndication_balances[_token];
        uint256 last_time = _token_last_time[_token];
        uint256 end = now.add(_duration);
        Range storage current_range = _balances[last_time];
        
        while ( now >= current_range.end ) {
            last_time = current_range.end;
            current_range = _balances[current_range.end];
        }
          
        if ( now > last_time ) {
            _balances[now] = Range(current_range.end, current_range.amount);
            current_range.end = now;
            current_range = _balances[now];
        }
              
        while ( current_range.end <= end ) {
            current_range.amount = current_range.amount.add(_amount);
            current_range = _balances[current_range.end];
        }
         
        if (current_range.end > end ) {
            _balances[end] = Range(current_range.end, current_range.amount);
            current_range.amount += _amount;
            current_range.end = end;
        }
        
        _token_last_time[_token] = now;
    }

    function _count_capital(address _token, uint256 _amount, uint256 _duration) internal {
        return;
        mapping(uint256 => Range) _balances = _token_capital_balances[_token];
        uint256 last_time = _token_capital_last_time[_token];
        uint256 end = now.add(_duration);
        Range storage current_range = _balances[last_time];

        
        while ( now >= current_range.end ) {
            last_time = current_range.end;
            current_range = _balances[current_range.end];
        }
          
        if ( now > last_time ) {
            _balances[now] = Range(current_range.end, current_range.amount);
            current_range.end = now;
            current_range = _balances[now];
        }
              
        while ( current_range.end <= end ) {
            current_range.amount = current_range.amount.add(_amount);
            current_range = _balances[current_range.end];
        }
         
        if (current_range.end > end ) {
            _balances[end] = Range(current_range.end, current_range.amount);
            current_range.amount += _amount;
            current_range.end = end;
        }
        
        _token_capital_last_time[_token] = now;
    }

    function _check_creditability(address _token, uint256 _amount, uint256 _duration) internal view returns(bool) {
        mapping(uint256 => Range) _balances = _token_syndication_balances[_token];
        uint256 last_time = _token_last_time[_token];
        Range storage current_range = _balances[last_time];
        uint256 end = now.add(_duration);


      
        while ( now >= current_range.end) {
            current_range = _balances[current_range.end];
        }

        while  ( current_range.end < end ) {
            if ( current_range.amount < _amount ) {
                return false;
            }
            current_range = _balances[current_range.end];
        }
      
        if ( current_range.amount < _amount) {
            return false;
        }
        return true;
    }

    function _check_depositability(address _token, uint256 _amount, uint256 _duration) internal view returns(bool) {
        mapping(uint256 => Range) _balances = _token_capital_balances[_token];
        uint256 last_time = _token_capital_last_time[_token];
        Range storage current_range = _balances[last_time];
        uint256 end = now.add(_duration);

        while ( now >= current_range.end) {
            current_range = _balances[current_range.end];
        }
      
        while  ( current_range.end < end ) {
            if ( current_range.amount < _amount ) {
                return false;
            }
            current_range = _balances[current_range.end];
        }

        if ( current_range.amount < _amount) {
            return false;
        }
        return true;
    }

    //синдикация конец

    //модификаторы

  

    //методы для банка
    
    //работа со внутренним счётом контракта - доступно только Owner-у
    function topUp(address _token, uint256 _amount) public onlyOwner {     //пополнить
        require(isTokenSupported(_token));
        require(IERC20(_token).allowance(msg.sender, this) >= _amount);
        require(IERC20(_token).transferFrom(msg.sender, this, _amount));

        emit ToppedUp(_amount);
    }

    function takeProfit(address _token, uint256 _amount) public onlyOwner {     //снять сумму
        require(isTokenSupported(_token));
        require(IERC20(_token).balanceOf(this) >= _amount);
        IERC20(_token).transfer(msg.sender, _amount);

        emit TookProfit(_amount);
    }
    //работа со внутренним счётом контракта конец

    //поддерживаемые токены начало
    function addTokenToSupportedTokens(address _tokenAddress, string _name, string _symbol) public onlyManager {
        require(!isTokenSupported(_tokenAddress));
        SupportedToken memory token = SupportedToken(true, _tokenAddress, _name, _symbol);
        supportedTokensMap[_tokenAddress] = token;
        supportedTokenAddresses.push(_tokenAddress);

        _token_syndication_balances[_tokenAddress][0] = Range(2**256 - 1, 0);

        _token_capital_balances[_tokenAddress][now] = Range(2**256 - 1, IERC20(_tokenAddress).balanceOf(this));
        _token_capital_last_time[_tokenAddress] = now;
    }
    
    function isTokenSupported(address _tokenAddress) public view returns (bool) {
        return supportedTokensMap[_tokenAddress].exists;
    }

    function getSupportedTokensCount() public view returns(uint256) {
        return supportedTokenAddresses.length;
    }
    //поддерживаемые токены конец

    //методы для blacklist
    function addToBlackList(address _client, uint256 _debt, uint256 _defaultDate) public {
        //require(_client == msg.sender || isManager(msg.sender));
        uint256 index = getBlackListCount();
        _blackListEntriesMap[_client] = BlackListEntry(index, _debt, _defaultDate);
        _blackList.push(_client);

        emit NewBlackListEntryAdded(index);
    }

    function deleteFromBlackList(address _client) public onlyManager {
        BlackListEntry memory blackListEntry = _blackListEntriesMap[_client];
        uint256 currentIndex = blackListEntry.index;

        //переносим последний на место currentIndex и обновляем его индекс
        address lastAddress = _blackList[_blackList.length - 1];
        _blackList[currentIndex] = lastAddress;
        BlackListEntry storage movedBlackListEntry = _blackListEntriesMap[lastAddress];
        movedBlackListEntry.index = currentIndex;

        //удаляем последний
        delete _blackList[_blackList.length - 1];

        emit BlackListEntryDeleted(currentIndex);
    }

    function getBlackListCount() public view returns(uint256){
        return _blackList.length;
    }
    //методы для blackList конец

    //методы для кредитного рейтинга начало
    function setCreditRating(address _client, uint16 _rating) public onlyManager {
        _setCreditRating(_client, _rating);
    }

    function _setCreditRating(address _client, uint16 _rating) internal {
        Client storage client = _clientsMap[_client];
        require(client.exists);
        client.rating = _rating;

        emit CreditRatingEdited(_rating);
    }

    //методы для кредитного 

    function getActualCreditProductCount() public view returns (uint256) {
        return actualCreditProductIds.length;
    }

    function _getAllCreditProductCount() private view returns (uint256) {
        return actualCreditProductIds.length + archivedCreditProductIds.length;
    }

    function getAllDepositProductCount() public view returns (uint256) {
        return actualDepositProductIds.length + archivedDepositProductIds.length;
    }
    
    function addCreditProduct(address _token, string _name,  uint256 _minAmount, uint256 _maxAmount, uint8 _minDuration, uint8 _maxDuration, uint16 _rate) external onlyManager { // добавить кредитный продукт
        require(isTokenSupported(_token));

        uint256 newActualCreditProductId = _getAllCreditProductCount();
        creditProductsMap[newActualCreditProductId] = CreditProduct(true, _token, newActualCreditProductId, _minAmount, _maxAmount, _minDuration, _maxDuration, _rate, _name);
        actualCreditProductIds.push(newActualCreditProductId);
        emit NewCreditProductAdded(newActualCreditProductId);
    }

    function addDepositProduct(address _token, string _name, uint256 _minAmount, uint256 _maxAmount, uint8 _minDuration, uint8 _maxDuration, uint16 _rate) external onlyManager {
        require(isTokenSupported(_token));

        uint256 newActualDepositProductId = getAllDepositProductCount();
        depositProductsMap[newActualDepositProductId] = DepositProduct(true, _token, newActualDepositProductId, _minAmount, _maxAmount, _minDuration, _maxDuration, _rate, _name);
        actualDepositProductIds.push(newActualDepositProductId);
        emit NewDepositProductAdded(newActualDepositProductId);
    }

    function archiveCreditProduct(uint256 _creditProductId) external onlyManager {
        CreditProduct storage creditProduct = creditProductsMap[_creditProductId];
        uint256 currentIndex = creditProduct.index;

        //переносим в archived и обновляем его статус и индекс
        uint256 newIndex = archivedCreditProductIds.push(_creditProductId) - 1;
        creditProduct.isActual = false;
        creditProduct.index = newIndex;

        //переносим последний id из actual на место currentIndex и обновляем его индекс
        uint256 lastId = actualCreditProductIds[actualCreditProductIds.length - 1];
        actualCreditProductIds[currentIndex] = lastId;
        CreditProduct storage movedCreditProduct = creditProductsMap[lastId];
        movedCreditProduct.index = currentIndex;

        //удаляем последний из actual
        delete actualCreditProductIds[actualCreditProductIds.length - 1];
    }

    function archiveDepositProduct(uint256 _depositProductId) external onlyManager {
        DepositProduct storage depositProduct = depositProductsMap[_depositProductId];
        uint256 currentIndex = depositProduct.index;

        //переносим в archived и обновляем его статус и индекс
        uint256 newIndex = archivedDepositProductIds.push(_depositProductId) - 1;
        depositProduct.isActual = false;
        depositProduct.index = newIndex;

        //переносим последний id из actual на место currentIndex и обновляем его индекс
        uint256 lastId = actualDepositProductIds[actualDepositProductIds.length - 1];
        actualDepositProductIds[currentIndex] = lastId;
        DepositProduct storage movedDepositProduct = depositProductsMap[lastId];
        movedDepositProduct.index = currentIndex;

        //удаляем последний из actual
        delete actualDepositProductIds[actualDepositProductIds.length - 1];
    }

    //работа с клиентами (добавляем клиента вручную в случае выдачи кредита)
    function addClient(address _address, uint16 _rating) public onlyManager {
        _addClient(_address, _rating);
    }

    function _addClient(address _address, uint16 _rating) internal {
        Client storage client = _clientsMap[_address];
        client.exists = true;
        client.rating = _rating;
        _clients.push(_address);

        emit ClientAdded(_address);
    }

    function getClientsCount() public view returns(uint256) {
        return _clients.length;
    }
    
        //депозитные  

    function _getAllDepositAccountCount() public view returns (uint256) {
        return _openedDepositAccountIds.length + _closedDepositAccountIds.length;    
    }

    function _createDepositAccount(uint256 _productId, uint256 _balance, uint256 _startBalance, uint8 _duration) internal returns (uint256) {
        uint256 newOpenedDepositAccountId = _getAllDepositAccountCount();           //создаём новый счёт и добавляем его id в массив счетов данного клиента
        depositAccountsMap[newOpenedDepositAccountId] = DepositAccount(true, msg.sender, newOpenedDepositAccountId, _productId, _balance, _startBalance, now, _duration);
        _openedDepositAccountIds.push(newOpenedDepositAccountId);


        Client storage client = _clientsMap[msg.sender];

        if (! client.exists) {
            _addClient(msg.sender, 0);
        }

        client.depositAccountIds.push(newOpenedDepositAccountId);

        return newOpenedDepositAccountId;
    }

    function _closeDepositAccount(uint256 _depositAccountId) internal {
        DepositAccount storage depositAccount = depositAccountsMap[_depositAccountId];
        uint256 currentIndex = depositAccount.index;

        //переносим в closed и обновляем его статус и индекс
        uint256 newIndex = _closedDepositAccountIds.push(_depositAccountId) - 1;
        depositAccount.isOpened = false;
        depositAccount.index = newIndex;

        //переносим последний id из opened на место currentIndex и обновляем его индекс
        uint256 lastId = _openedDepositAccountIds[_openedDepositAccountIds.length - 1];
        if (lastId != 0) {
            _openedDepositAccountIds[currentIndex] = lastId;
            DepositAccount storage movedDepositAccount = depositAccountsMap[lastId];
            movedDepositAccount.index = currentIndex;
        }

        //удаляем последний из opened
        _openedDepositAccountIds.length--;
    }


    function deposit(uint256 _productId, uint256 _amount, uint8 _duration) external { //разместить депозит

        DepositProduct memory product = depositProductsMap[_productId];
        address token = product.token;

        //рассчитываем сумму, которую нужно отдать, заранее
        uint256 balance = _amount.add(_amount.mul(uint256(product.rate)).div(10000));
        
        
        require(_check_depositability(token, balance, _duration));
        
        require(product.isActual); //проверяем что продукт актуальный
        require(_amount >= product.minAmount && _amount <= product.maxAmount);   //проверяем соответствие условиям продукта
        require(_duration >= product.minDuration && _duration <= product.maxDuration);
        
        require(IERC20(token).allowance(msg.sender, this) >= _amount);  //проверяем, разрешил ли клиент снять столько токенов и делаем перевод на счёт контракта
        require(IERC20(token).transferFrom(msg.sender, this, _amount));

        _count_product(token, _amount, _duration);
        _count_capital(token, balance, _duration);

        uint256 depositAccountId = _createDepositAccount(_productId, balance, _amount, _duration);   
        emit NewDepositOpen(msg.sender, depositAccountId);
    }

    //забрать депозит с процентами: пользователь или оператор должен вызвать эту функцию
    function withdraw(uint256 _depositAccountId) external {
        DepositAccount storage depositAccount = depositAccountsMap[_depositAccountId];
        DepositProduct memory product = depositProductsMap[depositAccount.productId];
        address token = product.token;
        address client = depositAccount.client;
        require(client == msg.sender || isManager(msg.sender));
        uint256 amount = 0;
        
        if (now >= depositAccount.startDate.add(uint256(depositAccount.duration).mul(1 minutes))) {
            amount = depositAccount.balance;
        }
        else {
            amount = depositAccount.startBalance;    
        }

        require(IERC20(token).balanceOf(this) >= amount);     //проверка, есть ли столько на счете контракта

        depositAccount.balance = 0;                
        _closeDepositAccount(_depositAccountId);
        IERC20(token).transfer(client, amount);

        if (now >= depositAccount.startDate.add(uint256(depositAccount.duration).mul(1 minutes))) {
            emit DepositWithdraw(client, _depositAccountId);
        }
        else {
            emit EarlyDepositWithdraw(client, _depositAccountId);
        }
    }


        // кредитные
    
    function _getAllCreditAccountCount() public view returns (uint256) {
        return _openedCreditAccountIds.length + _closedCreditAccountIds.length + _overdueCreditAccountIds.length;    
    }

    function _createCreditAccount(uint256 _productId, uint256 _amount, uint8 _duration) internal returns (uint256) {
        uint256 newOpenedCreditAccountId = _getAllCreditAccountCount();                                                                                         //создаём новый счёт и добавляем его id в массив счетов данного клиента
        creditAccountsMap[newOpenedCreditAccountId] = CreditAccount(CreditAccountState.Opened, msg.sender, newOpenedCreditAccountId, _productId, _amount, now, _duration);
        _openedCreditAccountIds.push(newOpenedCreditAccountId);

        Client storage client = _clientsMap[msg.sender];
        require(client.exists);
        require(_amount.mul(uint256(_duration)) <= uint256(client.rating));

        client.creditAccountIds.push(newOpenedCreditAccountId);

        return newOpenedCreditAccountId;
    }

    // возможные перемещения по state: 0 -> 1; 0 -> 2; 2 -> 1 (0 - opened; 1 - closed; 2 - overdue;)
    function _changeCreditAccountState(uint256 _creditAccountId, CreditAccountState _fromState, CreditAccountState _toState) internal {
        
        CreditAccount storage creditAccount = creditAccountsMap[_creditAccountId];
        uint256 currentIndex = creditAccount.index;
        
        //переносим в state (close или overdue) и обновляем его статус и индекс
        uint256 newIndex;
        if (_toState == CreditAccountState.Closed) {
            newIndex = _closedCreditAccountIds.push(_creditAccountId) - 1;
        }
        else if (_toState == CreditAccountState.Overdued) {
            newIndex = _overdueCreditAccountIds.push(_creditAccountId) - 1;
        }
        creditAccount.state = _toState;
        creditAccount.index = newIndex;

        //переносим последний id из opened или closed на место currentIndex и обновляем его индекс
        uint256 lastId;
        if (_fromState == CreditAccountState.Opened) {
                lastId = _openedCreditAccountIds[_openedCreditAccountIds.length - 1];
                if (lastId != 0) {
                    _openedCreditAccountIds[currentIndex] = lastId;
                }

            //удаляем последний из opened
            _openedCreditAccountIds.length--;

        } else if (_fromState == CreditAccountState.Overdued) {
            lastId = _overdueCreditAccountIds[_overdueCreditAccountIds.length - 1];
            if (lastId != 0) {
                _overdueCreditAccountIds[currentIndex] = lastId;
            }

            //удаляем последний из overdue
            _overdueCreditAccountIds.length--;
        }

        if (lastId != 0) {
            CreditAccount storage movedCreditAccount = creditAccountsMap[lastId];
        movedCreditAccount.index = currentIndex;
        }
    }

    function credit(uint256 _productId, uint256 _amount, uint8 _duration) external { //взять кредит
        CreditProduct memory product = creditProductsMap[_productId];
        address token = product.token;
        
        require(_check_creditability(token, _amount, _duration));
        require(product.isActual);                                                      //проверяем что продукт актуальный
        require(_amount >= product.minAmount && _amount <= product.maxAmount);         //проверяем соответствие условиям продукта
        require(_duration >= product.minDuration && _duration <= product.maxDuration);


        require(IERC20(token).balanceOf(this) >= _amount);                  //проверяем, есть ли столько на счету

        IERC20(token).transfer(msg.sender, _amount);

        //рассчитываем сумму, которую нам должны, заранее
        uint256 balance = _amount.add(_amount.mul(uint256(product.rate)).div(10000));

        //after transfer
        _count_product(token, _amount, _duration);

        uint256 creditAccountId = _createCreditAccount(_productId, balance, _duration);

        emit NewCreditOpen(msg.sender, creditAccountId);
    }

    
    function payback(uint256 _creditAccountId, uint256 _paybackAmount) external { //платёж по кредиту
        CreditAccount storage creditAccount = creditAccountsMap[_creditAccountId];
        address client = creditAccount.client;
        require(client == msg.sender || isManager(msg.sender));
        require(creditAccount.state != CreditAccountState.Closed);
        

        CreditProduct memory product = creditProductsMap[creditAccount.productId];

        address token = product.token;

        require(IERC20(token).allowance(client, this) >= _paybackAmount); //проверяем, что можно снять заявленную сумму

        uint256 amount = creditAccount.balance; //сколько должен
        uint256 currentTime = now;
        uint256 dueDate = creditAccount.startDate.add(uint256(creditAccount.duration));
        
        //отдаёт раньше срока
        if (currentTime <= dueDate) {

            if (_paybackAmount >= amount) {        //отдаёт сколько должен или больше
                require(IERC20(token).transferFrom(client, this, amount));
                creditAccount.balance = 0;

                _changeCreditAccountState(_creditAccountId, CreditAccountState.Opened, CreditAccountState.Closed);

                emit CreditClosed(client, _creditAccountId);
            }
            else {      //не хватает на счёте
                require(IERC20(token).transferFrom(client, this, _paybackAmount));
                creditAccount.balance = creditAccount.balance.sub(_paybackAmount);

                emit CreditPartiallyOffset(client, _creditAccountId);
            }
        } 
        //просрочил выплату кредита
        else {
            if (creditAccount.state == CreditAccountState.Opened) {     //обнаружение факта просрочки

                if (_paybackAmount >= amount) {        //отдаёт сколько должен или больше
                    require(IERC20(token).transferFrom(client, this, amount));
                    creditAccount.balance = 0;

                    _changeCreditAccountState(_creditAccountId, CreditAccountState.Opened, CreditAccountState.Closed);

                    emit CreditClosed(client, _creditAccountId);
                }
                else {      //не хватает на счёте
                    IERC20(token).transferFrom(client, this, _paybackAmount);
                    creditAccount.balance = creditAccount.balance.sub(_paybackAmount);
                    _changeCreditAccountState(_creditAccountId, CreditAccountState.Opened, CreditAccountState.Overdued);

                    emit CreditPartiallyOffset(client, _creditAccountId);
                }

                //заносим в BLACKLIST
                addToBlackList(client, creditAccount.balance, currentTime);
            }
            else if (creditAccount.state == CreditAccountState.Overdued) {    //возврат долгов по старой просрочке

                if (_paybackAmount >= amount) {        //отдаёт сколько должен или больше
                    IERC20(token).transferFrom(client, this, amount);
                    creditAccount.balance = 0;
                    _changeCreditAccountState(_creditAccountId, CreditAccountState.Overdued, CreditAccountState.Closed);

                    emit CreditClosed(client, _creditAccountId);
                }
                else {      //не хватает на счёте
                    IERC20(token).transferFrom(client, this, _paybackAmount);
                    creditAccount.balance = creditAccount.balance.sub(_paybackAmount);

                    emit CreditPartiallyOffset(client, _creditAccountId);
                }

                //уже есть в BLACKLIST
            }
        }
        
    }
    
}