pragma solidity ^0.4.24;

import "openzeppelin-solidity/contracts/access/Roles.sol";

contract Managable {
  using Roles for Roles.Role;

  event ManagerAdded(address indexed account);
  event ManagerRemoved(address indexed account);

  Roles.Role private _managers;

  constructor() internal {
    _addManager(msg.sender);
  }

  modifier onlyManager() {
    require(isManager(msg.sender));
    _;
  }

  function isManager(address account) public view returns (bool) {
    return _managers.has(account);
  }

  function addManager(address account) public onlyManager {
    _addManager(account);
  }

  function renounceManager(address account) public onlyManager {
    _removeManager(account);
  }

  function _addManager(address account) internal {
    _managers.add(account);
    emit ManagerAdded(account);
  }

  function _removeManager(address account) internal {
    _managers.remove(account);
    emit ManagerRemoved(account);
  }
}