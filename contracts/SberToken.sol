pragma solidity 0.4.24;

import "openzeppelin-solidity/contracts/token/ERC20/ERC20.sol";

//собственный токен для тестирования, см. https://ethereum.stackexchange.com/questions/61000/can-i-receive-use-an-already-existing-erc-20-token-in-a-new-smart-contract

contract SberToken is ERC20 {
    string public name = "SberToken";
    string public symbol = "SBER";
    uint256 public decimals = 18;

    uint256 public constant INITIAL_SUPPLY = 10000 * (10 ** uint256(decimals));

    constructor(uint256 _intialSupply) public {
        _mint(msg.sender, _intialSupply);
    }
}