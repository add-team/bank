var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

module.exports = function(deployer) {
  deployer.deploy(SberToken, 5000);
  deployer.deploy(Bank);
};
