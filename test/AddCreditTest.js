var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

function to(promise) {
   return promise.then(data => {
      return [data, null];
   })
   .catch(err => { 
   	return [null, err];
   });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let canFinish = null;

async function  wait(canFinish) {
	while(canFinish == null) {
		await sleep(1000);
	}
	console.log(canFinish);
}

contract("Bank", async (accounts) => {

	
	let deployer = accounts[0];
	

	it("credit", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;



		await to(tokenInstance.transfer(bankInstance.address, 100));

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(tx, "Error while token adding");

		[tx, err] = await to(bankInstance.addCreditProduct(tokenInstance.address, "КрутойКредит", 1, 10000, 1, 10, 2000));
		assert.ok(tx, "Error while credit product adding");

		[tx, err] = await to(bankInstance.addDepositProduct(tokenInstance.address, "Крутой", 1, 10000, 1, 10, 1000));
		assert.ok(tx, "Error while deposit product adding");

		await to(tokenInstance.approve(bankInstance.address, 100));

		await to(bankInstance.addClient(deployer, 10000));

		let event = bankInstance.NewDepositProductAdded([], {fromBlock:0, toBlock:'pending'});
		let depositProductId = null;

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event NewDepositOpen handling failed");
				}

				depositProductId = res.args.depositProductId.toNumber();
			});

		while(depositProductId == null) {
			await sleep(1000);
		}

		[tx, err] = await to(bankInstance.deposit(depositProductId, 3, 3));
		assert.ok(tx, "Error while deposit" + err);

		let depositAccountId = null;
		event = bankInstance.NewDepositOpen({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});
		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event NewDepositOpen handling failed");
			}
			depositAccountId = res.args.depositAccountId.toNumber();
		});

		while(depositAccountId == null) {
			await sleep(1000);
		}
		console.log('Deposit account: ', depositAccountId);




		event = bankInstance.NewCreditProductAdded([], {fromBlock:0, toBlock:'pending'});
		let creditProductId = null;

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event NewCreditOpen handling failed");
				}

				creditProductId = res.args.creditProductId.toNumber();
			});

		while(creditProductId == null) {
			await sleep(1000);
		}

		[tx, err] = await to(bankInstance.credit(creditProductId, 1, 1));
		assert.ok(tx, "Error while credit");

		let creditAccountId = null;
		event = bankInstance.NewCreditOpen({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});
		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event NewCreditOpen handling failed");
			}
			creditAccountId = res.args.creditAccountId.toNumber();
		});

		while(creditAccountId == null) {
			await sleep(1000);
		}
		console.log('Credit account Id: ', creditAccountId);

		//await sleep(60000);

		[tx, err] = await to(bankInstance.payback(creditAccountId, 3));
		assert.ok(tx, "Error while payback" + err);

		eventClosed = bankInstance.CreditClosed({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});

		let eventPartiallyOffset = bankInstance.CreditPartiallyOffset({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});

		let closedCreditAccountId = null;
		let overdueCredit = null;

		eventClosed.watch( async (err, res) => {
			eventClosed.stopWatching();
			if (err) {
				assert.fail("Event payback handling failed");
			}
			console.log(res);
			closedCreditAccountId = res.args.creditAccountId.toNumber();
		});

		eventPartiallyOffset.watch( async (err, res) => {
			eventPartiallyOffset.stopWatching();
			if (err) {
				assert.fail("Event eventPartiallyOffset handling failed");
			}
			console.log(res);
			overdueCredit = true;
		});

		while(closedCreditAccountId == null && overdueCredit == null) {
			await sleep(1000);
		}
		console.log('Closed credit account: ', closedCreditAccountId);
		console.log('PartiallyOffset: ', overdueCredit);
		
	});



});
