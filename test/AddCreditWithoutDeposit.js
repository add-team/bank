var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

function to(promise) {
   return promise.then(data => {
      return [data, null];
   })
   .catch(err => { 
   	return [null, err];
   });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let canFinish = null;

async function  wait(canFinish) {
	while(canFinish == null) {
		await sleep(100);
	}
	console.log(canFinish);
}

contract("Fail to take credit without deposit(Syndication)", async (accounts) => {

	
	let deployer = accounts[0];

	it("transfer 100 SberTokens to Bank contract", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		await to(tokenInstance.transfer(bankInstance.address, 100));
	});

	it("add SberToken as supported token to contract", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(tx, "Error while token adding");
	});

	it("transfer 100 SberTokens to client", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		await to(tokenInstance.transfer(accounts[1], 100));
	});


	it("approve token allowance to contract by client", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(tokenInstance.approve(bankInstance.address, 100, {from: accounts[1]}));
		assert.ok(tx, "Error while token approve" );

	});


	it("add new credit product 'Выгодный' ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		[tx, err] = await to(bankInstance.addCreditProduct(tokenInstance.address, "Выгодный", 1, 10000, 1, 10, 9000));
		assert.ok(tx, "Error while credit product adding");

	});

	let creditProductId = null;
	it("event on new credit product fired ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		let event = bankInstance.NewCreditProductAdded([], {fromBlock:0, toBlock:'pending'});
		

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event NewCreditProductAdded handling failed");
				}

				creditProductId = res.args.creditProductId.toNumber();
				console.log('New creditProductId:', creditProductId)
			});

		while(creditProductId == null) {
			await sleep(100);
		}
	});

	it("add client to whitelist with credit rating 10 ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		[tx, err] = await to(bankInstance.addClient(accounts[1], 10));
		assert.ok(tx, "Error while credit product adding");

	});

	let clientAddress = null;
	it("event on new client fired ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		let event = bankInstance.ClientAdded([], {fromBlock:'pending', toBlock:'pending'});
		

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event ClientAdded handling failed");
				}

				clientAddress = res.args.clientAddress;
				console.log('New clientAddress:', clientAddress)
			});

		while(clientAddress == null) {
			await sleep(100);
		}
	});



	it("fail to take credit by client without deposit", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		console.log('Tokens on client account = ', (await tokenInstance.balanceOf(accounts[1])).toNumber(), ' before');
		[tx, err] = await to(bankInstance.credit(creditProductId, 2	, 1, {from: accounts[1]}));
		assert.ok(err, "Error while credit" + err);	

	});




});
