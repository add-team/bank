var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

function to(promise) {
   return promise.then(data => {
      return [data, null];
   })
   .catch(err => { 
   	return [null, err];
   });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let canFinish = null;

async function  wait(canFinish) {
	while(canFinish == null) {
		await sleep(1000);
	}
	console.log(canFinish);
}

contract("Bank", async (accounts) => {

	
	let deployer = accounts[0];
	

	it("deposit", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		await to(tokenInstance.transfer(bankInstance.address, 100));

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(tx, "Error while token adding");

		[tx, err] = await to(bankInstance.addCreditProduct(tokenInstance.address, "КрутойКредит", 1, 10000, 1, 10, 2000));
		assert.ok(tx, "Error while credit product adding");

		[tx, err] = await to(bankInstance.addDepositProduct(tokenInstance.address, "Крутой", 1, 10000, 1, 10, 1000));
		assert.ok(tx, "Error while deposit product adding");

		await to(tokenInstance.approve(bankInstance.address, 100));

		await to(bankInstance.addClient(deployer, 10000));

		let event = bankInstance.NewDepositProductAdded([], {fromBlock:0, toBlock:'pending'});
		let depositProductId = null;

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event NewDepositOpen handling failed");
				}

				depositProductId = res.args.depositProductId.toNumber();
			});

		while(depositProductId == null) {
			await sleep(1000);
		}

		[tx, err] = await to(bankInstance.deposit(depositProductId, 3, 3));
		assert.ok(tx, "Error while deposit" + err);

		let depositAccountId = null;
		event = bankInstance.NewDepositOpen({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});
		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event NewDepositOpen handling failed");
			}
			depositAccountId = res.args.depositAccountId.toNumber();
		});

		while(depositAccountId == null) {
			await sleep(1000);
		}
		console.log('Deposit account: ', depositAccountId);
					

		await sleep(60000);

		[tx, err] = await to(bankInstance.withdraw(depositAccountId));
		assert.ok(tx, "Error while withdraw");
		event = bankInstance.DepositWithdraw({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});

		let earlyDepositWithdrawEffortEvent = bankInstance.EarlyDepositWithdrawEffort({client: accounts[0]}, {fromBlock:0, toBlock:'pending'});

		let closedDepositAccountId = null;
		let earlyDepositWithdrawEffort = null;

		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event DepositWithdraw handling failed");
			}
			closedDepositAccountId = res.args.depositAccountId.toNumber();
		});

		earlyDepositWithdrawEffortEvent.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event EarlyDepositWithdrawEffort handling failed");
			}
			earlyDepositWithdrawEffort = true;
		});

		while(closedDepositAccountId == null && earlyDepositWithdrawEffort == null) {
			await sleep(1000);
		}
		console.log('Closed deposit account: ', closedDepositAccountId);
		console.log('EarlyDepositWithdrawEffort: ', earlyDepositWithdrawEffort);
		
	});



});
