var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

function to(promise) {
   return promise.then(data => {
      return [data, null];
   })
   .catch(err => { 
   	return [null, err];
   });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

let canFinish = null;

async function  wait(canFinish) {
	while(canFinish == null) {
		await sleep(100);
	}
	console.log(canFinish);
}

contract("Take deposit and withdraw at time", async (accounts) => {

	
	let deployer = accounts[0];

	it("transfer 100 SberTokens to Bank contract", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		await to(tokenInstance.transfer(bankInstance.address, 100));
	});

	it("add SberToken as supported token to contract", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(tx, "Error while token adding");
	});

	it("transfer 100 SberTokens to client", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		await to(tokenInstance.transfer(accounts[1], 100));
	});


	it("add new deposit product 'Рекордный' ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;
		
		[tx, err] = await to(bankInstance.addDepositProduct(tokenInstance.address, "Рекордный", 1, 10000, 1, 10, 9000));
		assert.ok(tx, "Error while deposit product adding");
	});

	let depositProductId = null;
	it("event on new deposit product fired ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		let event = bankInstance.NewDepositProductAdded([], {fromBlock:0, toBlock:'pending'});
		

		event.watch( async (err, res) => {
			event.stopWatching();
				if (err) {
					assert.fail("Event NewDepositOpen handling failed");
				}

				depositProductId = res.args.depositProductId.toNumber();
				console.log('New depositProductId:', depositProductId)
			});

		while(depositProductId == null) {
			await sleep(100);
		}
	});

	it("approve token allowance to contract by client", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(tokenInstance.approve(bankInstance.address, 100, {from: accounts[1]}));
		assert.ok(tx, "Error while token approve" );

	});

	it("let client make deposit", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		console.log('Tokens on client account = ', (await tokenInstance.balanceOf(accounts[1])).toNumber(), ' before');
		[tx, err] = await to(bankInstance.deposit(depositProductId, 3, 1, {from: accounts[1]}));
		assert.ok(tx, "Error while deposit" + err);	

	});

	let depositAccountId = null;
	it("event on new deposit account fired ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		
		event = bankInstance.NewDepositOpen({client: accounts[1]}, {fromBlock:'pending', toBlock:'pending'});
		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event NewDepositOpen handling failed");
			}
			depositAccountId = res.args.depositAccountId.toNumber();
			console.log('New depositAccountId:', depositAccountId)
			console.log('Tokens on client account = ', (await tokenInstance.balanceOf(accounts[1])).toNumber());
		});

		while(depositAccountId == null) {
			await sleep(100);
		}
	});


	it("let client withdraw deposit at time", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		console.log('Sleep 65 seconds');
		await sleep(65000);
		[tx, err] = await to(bankInstance.withdraw(depositAccountId, {from: accounts[1]}));
		assert.ok(tx, "Error while deposit" + err);	

	});


	it("event on withdraw account fired ", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		depositAccountId = null;
		event = bankInstance.DepositWithdraw({client: accounts[1]}, {fromBlock:'latest', toBlock:'pending'});
		event.watch( async (err, res) => {
			event.stopWatching();
			if (err) {
				assert.fail("Event DepositWithdraw handling failed");
			}
			
			console.log('Deposit with depositAccountId = ', res.args.depositAccountId.toNumber(), ' withdrawed at time');
			console.log('Tokens on client account = ', (await tokenInstance.balanceOf(accounts[1])).toNumber(), '  after withdraw at time');
			depositAccountId = res.args.depositAccountId.toNumber();
		});

		while(depositAccountId == null) {
			await sleep(100);
		}
	});

});
