var SberToken = artifacts.require("./SberToken.sol");
var Bank = artifacts.require("./Bank.sol");

function to(promise) {
   return promise.then(data => {
      return [data, null];
   })
   .catch(err => { 
   	return [null, err];
   });
}

contract("Bank", async (accounts) => {

	
	let deployer = accounts[0];
	

	it("add supported token", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		let supportedTokensCount = (await bankInstance.getSupportedTokensCount()).toNumber();
		assert.equal(0, supportedTokensCount, "Initial number of supported tokens greater than 0");

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(tx, "Error while token adding");
		supportedTokensCount = (await bankInstance.getSupportedTokensCount()).toNumber();
		assert.equal(1, supportedTokensCount, "Number of supported tokens after first token addition not equal to 1");
	});

	it("add same supported token one more time", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT"));
		assert.ok(err, "Ok while one more token adding");
		let supportedTokensCount = (await bankInstance.getSupportedTokensCount()).toNumber();
		assert.equal(1, supportedTokensCount, "Number of supported tokens after the same token addition not equal to 1");
	});

	it("try to add supported token as not manager", async () => {

		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		
		let err, tx;

		[tx, err] = await to(bankInstance.addTokenToSupportedTokens(tokenInstance.address, "SberToken", "SBT", {from: accounts[2]}));
		assert.ok(err, "Ok while unauthorized adding");
	});


	it("add credit product with supported token", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addCreditProduct(tokenInstance.address, "Cool", 1, 10000, 1, 10, 1000));
		assert.ok(tx, "Error while credit product adding");
	});

	it("add credit product with unsupported token", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addCreditProduct(accounts[5], "Морковный", 1, 10000, 1, 10, 1000));
		assert.ok(err, "Ok while credit product adding");
	});

	it("add credit product with supported token as not manager", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addCreditProduct(tokenInstance.address, "Лучший", 1, 10000, 1, 10, 1000, {from: accounts[5]}));
		assert.ok(err, "Ok while credit product adding");
	});


	it("add deposit product with supported token", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addDepositProduct(tokenInstance.address, "Крутой", 1, 10000, 1, 10, 1000));
		assert.ok(tx, "Error while deposit product adding" + err);
	});

	it("add deposit product with unsupported token", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addDepositProduct(accounts[5], "Best", 1, 10000, 1, 10, 1000));
		assert.ok(err, "Ok while deposit product adding");
	});

	it("add deposit product with supported token as not manager", async () => {
		
		let tokenInstance = await SberToken.deployed();
		let bankInstance = await Bank.deployed();
		let err, tx;

		[tx, err] = await to(bankInstance.addDepositProduct(tokenInstance.address, "Bad", 1, 10000, 1, 10, 1000, {from: accounts[5]}));
		assert.ok(err, "Ok while deposit product adding");
	});

});
