var SberToken = artifacts.require("./SberToken.sol");

contract("SberToken", function (sendToken){
	it("check first account balance", function() {
	  return SberToken.deployed().then(function(app){
	  	return app.balanceOf(web3.eth.accounts[0]);
	  }).then(function(balance) {
	  	assert.equal(balance, 5000);
	  });
	});
});
